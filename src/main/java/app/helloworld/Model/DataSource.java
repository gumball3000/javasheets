package app.helloworld.Model;

import app.helloworld.SendEmailSSL;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import app.helloworld.ObjectProperty.LoadDetailsProperty;
import app.helloworld.SheetsAPI;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.*;
import javafx.beans.property.SimpleStringProperty;
import org.apache.commons.lang.StringUtils;


import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

public class DataSource {
    private NetHttpTransport HTTP_TRANSPORT;
    private String spreadsheetId;
    private String milesId;
    private String loadoardId;
    //private String range;
    private Sheets service;
    private List<LoadDetailsProperty> loads;




    private static DataSource instance;

    static {
        try {
            instance = new DataSource();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private DataSource() throws Exception{
        SendEmailSSL email = new SendEmailSSL();
        File configFile = new File( "config.properties" );

        try {
            FileReader reader = new FileReader(configFile);
            Properties props = new Properties();
            props.load(reader);
            spreadsheetId = props.getProperty("loads");
            milesId = props.getProperty("miles");
            loadoardId = props.getProperty("loadboard");
            reader.close();
        } catch (FileNotFoundException ex) {
            // file does not exist
        } catch (IOException ex) {
            // I/O error
        }


        PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
        config.setProperty("company1", "Crunchify");
        config.save();


        HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        //spreadsheetId = "1g2hhmTvrvEhhLkVyqNYLDBNEzHG6o-V7_mb0HrHMF00";
        service = new Sheets.Builder(HTTP_TRANSPORT, SheetsAPI.JSON_FACTORY, SheetsAPI.getCredentials(HTTP_TRANSPORT))
                .setApplicationName(SheetsAPI.APPLICATION_NAME)
                .build();
    }

    public static DataSource getInstance() {
        return instance;
    }


    public List<LoadDetailsProperty> queryLoadDetails() throws Exception{
        // Build a new authorized API client service.
        System.out.println("wanna try again");
//
//        String range = "DETAILS!A1:S";
//        ValueRange response = service.spreadsheets().values()
//                .get(spreadsheetId, range).execute();
//        List<List<Object>> values = response.getValues();
        List<String> ranges = new ArrayList<>();
        ranges.add( "A1:R" );
        //ranges.add( "D1:E" );
        Sheets.Spreadsheets.Get request = service.spreadsheets().get(spreadsheetId);
        request.setIncludeGridData(true);
        request.setRanges( ranges );
        Spreadsheet spreadsheet = request.execute();
        List<RowData> values =spreadsheet.getSheets().get(0).getData().get(0).getRowData();

//        ValueRange body = new ValueRange()
//                .setValues(Arrays.asList(
//                        Arrays.asList("Expenses January"),
//                        Arrays.asList("books", "30"),
//                        Arrays.asList("pens", "10"),
//                        Arrays.asList("Expenses February"),
//                        Arrays.asList("clothes", "20"),
//                        Arrays.asList("shoes", "10/22/19","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD","NO","NO","DELIVERED + POD","DELIVERED + POD","DELIVERED + POD")));
//        UpdateValuesResponse result = service.spreadsheets().values()
//                .update(spreadsheetId, "A1476", body)
//                .setValueInputOption("RAW")
//                .execute();

        //String color = spreadsheet.getSheets().get(0).getData().get(0).getRowData().get(953).getValues().get(0).getEffectiveFormat().getBackgroundColor().toString();
        //String substringBetween = StringUtils.substringBetween(color, "\"", "\"");
        //System.out.println(substringBetween);
        //System.out.println(spreadsheet.getSheets().get(0).getData().get(0).getRowData().get(1462).getValues().get(15).getEffectiveValue());
        //System.out.println(spreadsheet);


        loads = new ArrayList<>();
        if (spreadsheet == null || spreadsheet.isEmpty()) {
            System.out.println("No data found.");
        } else {
            System.out.println("got here1");
            //System.out.println("Name, Major");
            int i = 1;
            for (RowData row : values) {
                //System.out.println("got here1");
                //System.out.printf( "%s %s, %s\n", row.get( 0 ), row.get( 1 ), row.get( 2 ) );
                // Print columns A and E, which correspond to indices 0 and 4.
                //if (row.size() == 16) {
                    LoadDetailsProperty loadRow = new LoadDetailsProperty();
                    loadRow.setLine(Integer.toString( i++ ));
                    loadRow.setTractor( (String) row.getValues().get(0).getFormattedValue() );
                    loadRow.setLoad((String)row.getValues().get(1).getFormattedValue());
                    loadRow.setBroker((String)row.getValues().get(2).getFormattedValue());
                    loadRow.setPuLocation((String)row.getValues().get(3).getFormattedValue());
                    loadRow.setDeLocation( (String) row.getValues().get(4).getFormattedValue());
                    loadRow.setPuDate((String)row.getValues().get(5).getFormattedValue());
                    loadRow.setPuAppt( (String)row.getValues().get(6).getFormattedValue() );
                    loadRow.setPuIn( (String) row.getValues().get(7).getFormattedValue());
                    loadRow.setPuOut((String) row.getValues().get(8).getFormattedValue());
                    loadRow.setDeDate((String)row.getValues().get(9).getFormattedValue());
                    loadRow.setDeAppt( (String) row.getValues().get(10).getFormattedValue());
                    loadRow.setDeIn((String) row.getValues().get(11).getFormattedValue());
                    loadRow.setDeOut((String)  row.getValues().get(12).getFormattedValue());
                    loadRow.setLumper((String)row.getValues().get(13).getFormattedValue());
                    loadRow.setDetention( (String)row.getValues().get(14).getFormattedValue());
                    loadRow.setStatus((String) row.getValues().get(15).getFormattedValue());
                    loadRow.setIncome((String) row.getValues().get(16).getFormattedValue());
                    loadRow.setNote( (String)row.getValues().get(17).getFormattedValue());
                    loads.add( loadRow );

                //System.out.println("got here5");
                    //System.out.printf( "%d %s, %s\n", row.get( 0 ), row.get( 1 ) );

                //}

            }
        }
        System.out.println("done");
        return loads;
    }

    private int getFirstEmptyRow(List<LoadDetailsProperty> rows) throws Exception{
        if (rows.isEmpty()){
            return -1;
        } else {
            boolean x = false;
            ListIterator listIterator = rows.listIterator(rows.size());
            while (listIterator.hasPrevious()) {
                LoadDetailsProperty prev = (LoadDetailsProperty) listIterator.previous();
                if (rows.get(rows.indexOf(prev)).getTractor() != null){
                    if (rows.get(rows.indexOf(prev)).getTractor().trim().length()==0){
                        listIterator.next();
                        listIterator.next();
                        return (rows.indexOf( listIterator.next() ));
                    } else {
                        listIterator.next();
                        listIterator.next();
                        return (rows.indexOf( listIterator.next() ));
                    }
                }

            }
        }
        return -1;
    }



    public BatchUpdateValuesResponse addNewLoad(List<List<List<String>>> updateList)throws Exception{
        queryLoadDetails();
        int index = getFirstEmptyRow( loads );
        List<ValueRange> data = new ArrayList<>();
        data.add(new ValueRange()
                .setRange("A"+index)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(0).toArray()))));
        data.add(new ValueRange()
                .setRange("J"+index)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(1).toArray()))));
        data.add(new ValueRange()
                .setRange("N"+index)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(2).toArray()))));

        BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
                .setValueInputOption("USER_ENTERED")
                .setData(data);

        BatchUpdateValuesResponse batchResult = service.spreadsheets().values()
                .batchUpdate(spreadsheetId, batchBody)
                .execute();
        return batchResult;
    }

    public void addNewLoadToMilesTable(List<List<List<String>>> updateList, String rangeLetter, int rangeNumber)throws Exception{
        List<ValueRange> data = new ArrayList<>();
        data.add(new ValueRange()
                .setRange("November 2019!" + rangeLetter + rangeNumber)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(0).toArray()))));
        //char ch = rangeLetter.charAt( rangeLetter.length()-1 );
        String newRangeLetter;
        if (rangeLetter.length()==2) {
            newRangeLetter = rangeLetter.substring( 0, 1 ) + ((char) ((int) rangeLetter.charAt( rangeLetter.length() - 1 ) + 6));
        } else if (rangeLetter.equalsIgnoreCase( "z" )){
            newRangeLetter = "AF";
        } else {
            newRangeLetter = String.valueOf((char) ((int) rangeLetter.charAt( rangeLetter.length() - 1 ) + 6));
        }
        data.add(new ValueRange()
                .setRange("November 2019!" + newRangeLetter + rangeNumber)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(1).toArray()))));


        BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
                .setValueInputOption("USER_ENTERED")
                .setData(data);

        BatchUpdateValuesResponse batchResult = service.spreadsheets().values()
                .batchUpdate(milesId, batchBody)
                .execute();
    }

    public BatchUpdateValuesResponse editLoad(List<List<List<String>>> updateList, String indexString)throws Exception{
        int index = Integer.parseInt( indexString );
        List<ValueRange> data = new ArrayList<>();
        data.add(new ValueRange()
                .setRange("A"+index)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(0).toArray()))));
        data.add(new ValueRange()
                .setRange("J"+index)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(1).toArray()))));
        data.add(new ValueRange()
                .setRange("N"+index)
                .setValues(Arrays.asList(
                        Arrays.asList(updateList.get(2).toArray()))));

        BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
                .setValueInputOption("USER_ENTERED")
                .setData(data);

        BatchUpdateValuesResponse batchResult = service.spreadsheets().values()
                .batchUpdate(spreadsheetId, batchBody)
                .execute();
        return batchResult;
    }

    public void updateInLoadboard(LoadDetailsProperty item) throws Exception{
        String[] str1 = {item.getDeLocation()};
        String[] str2 = {item.getDeDate(), item.getDeAppt()};
        int index = queryLoadboard( item.getTractor() );
        if (index>0){
            List<ValueRange> data = new ArrayList<>();
            data.add(new ValueRange()
                    .setRange("H"+index)
                    .setValues(Arrays.asList(
                            Arrays.asList(str1))));
            data.add(new ValueRange()
                    .setRange("J"+index)
                    .setValues(Arrays.asList(
                            Arrays.asList(str2))));
            BatchUpdateValuesRequest batchBody = new BatchUpdateValuesRequest()
                    .setValueInputOption("USER_ENTERED")
                    .setData(data);

            BatchUpdateValuesResponse batchResult = service.spreadsheets().values()
                    .batchUpdate(loadoardId, batchBody)
                    .execute();
        }
    }

    public int queryLoadboard(String tractorNumber) throws Exception{
        System.out.println("got here2");
        List<String> ranges = new ArrayList<>();
        ranges.add( "C1:D" );
        Sheets.Spreadsheets.Get request = service.spreadsheets().get(loadoardId);
        request.setIncludeGridData(true);
        request.setRanges( ranges );
        Spreadsheet spreadsheet = request.execute();
        List<RowData> values =spreadsheet.getSheets().get(0).getData().get(0).getRowData();
        loads = new ArrayList<>();
        if (spreadsheet == null || spreadsheet.isEmpty()) {
        } else {
            int i = 1;
            for (RowData row : values) {
                //System.out.println(row.getValues().get(0).getFormattedValue());
                if (tractorNumber.equals( row.getValues().get(0).getFormattedValue())){
                    return i++;
                }
            }
        }
        System.out.println("done");
        return 0;
    }



}
