package app.helloworld.controller;


import app.helloworld.Model.DataSource;
import app.helloworld.ObjectProperty.LoadDetailsProperty;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesResponse;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class HelloWorldController implements Initializable {

    @FXML private TextField filterField;
    @FXML private Button b1;
    @FXML private Button b2;
    @FXML private Button b3;
    @FXML private Button b4;
    @FXML private TableView loads;
    @FXML private TableColumn<LoadDetailsProperty, String> tractor;
    @FXML private TableColumn<LoadDetailsProperty, String> load;
    @FXML private TableColumn<LoadDetailsProperty, String> broker;
    @FXML private TableColumn<LoadDetailsProperty, String> puLocation;
    @FXML private TableColumn<LoadDetailsProperty, String> deLocation;
    @FXML private TableColumn<LoadDetailsProperty, String> puDate;
    @FXML private TableColumn<LoadDetailsProperty, String> puAppt;
    @FXML private TableColumn<LoadDetailsProperty, String> puIn;
    @FXML private TableColumn<LoadDetailsProperty, String> puOut;
    @FXML private TableColumn<LoadDetailsProperty, String> deDate;
    @FXML private TableColumn<LoadDetailsProperty, String> deAppt;
    @FXML private TableColumn<LoadDetailsProperty, String> deIn;
    @FXML private TableColumn<LoadDetailsProperty, String> deOut;
    @FXML private TableColumn<LoadDetailsProperty, String> lumper;
    @FXML private TableColumn<LoadDetailsProperty, String> detention;
    @FXML private TableColumn<LoadDetailsProperty, String> status;
    @FXML private TableColumn<LoadDetailsProperty, String> income;
    @FXML private TableColumn<LoadDetailsProperty, String> note;
    @FXML private ProgressIndicator pbar;

    private ObservableList<LoadDetailsProperty> masterData;
    private FilteredList<LoadDetailsProperty> filteredData;
    private String lowerCaseFilter;
    private SortedList<LoadDetailsProperty> sortedData;


    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        pbar.setProgress( -1.0 );
        pbar.setProgress( -1.0 );
        pbar.setVisible( false );
        listLoads();
    }



    @FXML
    public void listLoads(){
        //if (loads.getItems().isEmpty()){
        //loads.refresh();
        loads.setEditable( true );
        b1.setDisable( true );
        b2.setDisable( true );
        b3.setDisable( true );
        b4.setDisable( true );
        pbar.setVisible( true );
            Task<ObservableList<LoadDetailsProperty>> task = new Task<ObservableList<LoadDetailsProperty>>() {
                @Override
                protected ObservableList<LoadDetailsProperty> call() throws Exception {
                    return FXCollections.observableArrayList( DataSource.getInstance().queryLoadDetails() );
                }
            };
            task.setOnSucceeded( t->{
                tractor.setCellValueFactory(cellData -> cellData.getValue().tractorProperty());
                load.setCellValueFactory(cellData -> cellData.getValue().loadProperty());
                broker.setCellValueFactory(cellData -> cellData.getValue().brokerProperty());
                puLocation.setCellValueFactory(cellData -> cellData.getValue().puLocationProperty());
                deLocation.setCellValueFactory(cellData -> cellData.getValue().deLocationProperty());
                puDate.setCellValueFactory(cellData -> cellData.getValue().puDateProperty());
                puAppt.setCellValueFactory(cellData -> cellData.getValue().puApptProperty());
                puIn.setCellValueFactory(cellData -> cellData.getValue().puInProperty());
                puOut.setCellValueFactory(cellData -> cellData.getValue().puOutProperty());
                deDate.setCellValueFactory(cellData -> cellData.getValue().deDateProperty());
                deAppt.setCellValueFactory(cellData -> cellData.getValue().deApptProperty());
                deIn.setCellValueFactory(cellData -> cellData.getValue().deInProperty());
                deOut.setCellValueFactory(cellData -> cellData.getValue().deOutProperty());
                lumper.setCellValueFactory(cellData -> cellData.getValue().lumperProperty());
                detention.setCellValueFactory(cellData -> cellData.getValue().detentionProperty());
                status.setCellValueFactory(cellData -> cellData.getValue().statusProperty());
                income.setCellValueFactory(cellData -> cellData.getValue().incomeProperty());
                note.setCellValueFactory(cellData -> cellData.getValue().noteProperty());

                tractor.setCellFactory( TextFieldTableCell.forTableColumn());
                load.setCellFactory( TextFieldTableCell.forTableColumn());
                broker.setCellFactory( TextFieldTableCell.forTableColumn());
                puLocation.setCellFactory( TextFieldTableCell.forTableColumn());
                deLocation.setCellFactory( TextFieldTableCell.forTableColumn());
                puDate.setCellFactory( TextFieldTableCell.forTableColumn());
                puAppt.setCellFactory( TextFieldTableCell.forTableColumn());
                puIn.setCellFactory( TextFieldTableCell.forTableColumn());
                puOut.setCellFactory( TextFieldTableCell.forTableColumn());
                deDate.setCellFactory( TextFieldTableCell.forTableColumn());
                deAppt.setCellFactory( TextFieldTableCell.forTableColumn());
                deIn.setCellFactory( TextFieldTableCell.forTableColumn());
                deOut.setCellFactory( TextFieldTableCell.forTableColumn());
                lumper.setCellFactory( TextFieldTableCell.forTableColumn());
                detention.setCellFactory( TextFieldTableCell.forTableColumn());
                status.setCellFactory( TextFieldTableCell.forTableColumn());
                income.setCellFactory( TextFieldTableCell.forTableColumn());
                note.setCellFactory( TextFieldTableCell.forTableColumn());
                //note.setCellFactory( TextFieldTableCell.forTableColumn());
                //note.setEditable( true );
                masterData = task.getValue();
                filteredData = new FilteredList<>(masterData, p -> true);
                filterField.textProperty().addListener((observable, oldValue, newValue) -> {
                    filteredData.setPredicate(loadRow -> {
                        // If filter text is empty, display all persons.
                        if (newValue == null || newValue.isEmpty()) {
                            return true;
                        }

                        // Compare first name and last name of every person with filter text.
                        lowerCaseFilter = newValue.toLowerCase();
                        if (loadRow.getTractor() != null && loadRow.getBroker() != null ) {
                            if (loadRow.getTractor() != null) {
                                if (loadRow.getTractor().toLowerCase().contains( lowerCaseFilter )) {
                                    return true;
                                }
                            }

                            if (loadRow.getLoad() != null) {
                                if (loadRow.getLoad().toLowerCase().contains( lowerCaseFilter )) {
                                    return true;
                                }
                            }

                            if (loadRow.getBroker() != null) {
                                if (loadRow.getBroker().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getPuLocation() != null) {
                                if (loadRow.getPuLocation().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getDeLocation() != null) {
                                if (loadRow.getDeLocation().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getPuDate() != null) {
                                if (loadRow.getPuDate().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getPuAppt() != null) {
                                if (loadRow.getPuAppt().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getPuIn() != null) {
                                if (loadRow.getPuIn().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getPuOut() != null) {
                                if (loadRow.getPuOut().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getDeDate() != null) {
                                if (loadRow.getDeDate().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getDeAppt() != null) {
                                if (loadRow.getDeAppt().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getDeIn() != null) {
                                if (loadRow.getDeIn().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getDeOut() != null) {
                                if (loadRow.getDeOut().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getLumper() != null) {
                                if (loadRow.getLumper().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getDetention() != null) {
                                if (loadRow.getDetention().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getStatus() != null) {
                                if (loadRow.getStatus().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getIncome() != null) {
                                if (loadRow.getIncome().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                            if (loadRow.getNote() != null) {
                                if (loadRow.getNote().toLowerCase().contains( lowerCaseFilter )) {
                                    return true; // Filter matches last name.
                                }
                            }

                        }
                        return false; // Does not match.
                    });
                });

                // 3. Wrap the FilteredList in a SortedList.
                sortedData = new SortedList<>(filteredData);

                // 4. Bind the SortedList comparator to the TableView comparator.
                // 	  Otherwise, sorting the TableView would have no effect.
                sortedData.comparatorProperty().bind(loads.comparatorProperty());

                // 5. Add sorted (and filtered) data to the table.
                loads.setItems(sortedData);
//                loads.setRowFactory(tv -> new TableRow<LoadDetailsProperty>() {
//                    @Override
//                    public void updateItem(LoadDetailsProperty item, boolean empty) {
//                        super.updateItem(item, empty) ;
//                        if (item == null) {
//                            setStyle("");
//                        } else if (item.getStatus() != null) {
//                            if (item.getStatus().equals("DELIVERED + POD")) {
//                                setStyle( "-fx-background-color: green;" );
//                            } else {
//                                setStyle("");
//                            }
//                        } else {
//                            setStyle("");
//                    }
//                    }
//                });
//                note.setCellFactory(col -> {
//                    TableCell<SimpleStringProperty, Boolean> cell = new TableCell<LoadDetailsProperty, Boolean>() {
//                        @Override
//                        public void updateItem(Boolean item, boolean empty) {
//                            super.updateItem(item, empty);
//
//                            if (item) {
//                                this.setBackgroundColor(Color.RED);
//                            } else {
//                                this.setBackgroundColor(Color.GREEN);
//                            }
//                        }
//                    };
//                    return cell;
//                });
                pbar.setVisible(false);
                b1.setDisable( false );
                b2.setDisable( false );
                b3.setDisable( false );
                b4.setDisable( false );

            } );

        //masterData = task.getValue();
            //loads.itemsProperty().bind( task.valueProperty() );
            new Thread( task ).start();






       // }
    }

    @FXML
    public void addNewLoadDialog(){
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner( loads.getScene().getWindow() );
        dialog.setTitle( "Add new Load" );
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation( getClass().getResource( "/app/helloworld/view/AddNewLoadDialog.fxml" ) );
        try {
            dialog.getDialogPane().setContent( fxmlLoader.load() );
        } catch (IOException e) {
            System.out.println( "Couldn't load the dialog" );
            e.printStackTrace();
            return;
        }
        final AddNewLoadDialogController controller = fxmlLoader.getController();
        dialog.getDialogPane().getButtonTypes().add( ButtonType.CANCEL );
        dialog.getDialogPane().getButtonTypes().add( ButtonType.OK );


        final Button okButton = (Button) dialog.getDialogPane().lookupButton( ButtonType.OK );
        okButton.addEventFilter( ActionEvent.ACTION, ae -> {
            if (controller.checkFields()) {
                ae.consume(); //not valid
            } else {
            }
        } );

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            pbar.setVisible( true );
            b1.setDisable( true );
            b2.setDisable( true );
            b3.setDisable( true );
            b4.setDisable( true );
            try {
                Task<BatchUpdateValuesResponse> task = new Task<BatchUpdateValuesResponse>() {
                    @Override
                    protected BatchUpdateValuesResponse call() throws Exception {
                        return DataSource.getInstance().addNewLoad( controller.processResults() );
                    }
                };
                task.setOnSucceeded( t->{
//                    pbar.setVisible( false);
//                    b1.setDisable( false );
//                    b2.setDisable( false );
//                    b3.setDisable( false );
//                    b4.setDisable( false );
                    listLoads();
                });
                new Thread( task ).start();
            } catch (Exception e) {
                pbar.setVisible( false );
                b1.setDisable( false );
                b2.setDisable( false );
                b3.setDisable( false );
                b4.setDisable( false );
                e.printStackTrace();
            }

            System.out.println( "ok pressed" );
        } else {
            System.out.println( "cancel pressed" );
        }

    }

    @FXML
    public void editLoadDialog() throws Exception{
        final LoadDetailsProperty item = (LoadDetailsProperty) loads.getSelectionModel().getSelectedItem();
        //DataSource.getInstance().updateInLoadboard(item);
        if (item != null) {
            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.initOwner( loads.getScene().getWindow() );
            dialog.setTitle( "Edit Load" );
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation( getClass().getResource( "/app/helloworld/view/AddNewLoadDialog.fxml" ) );
            try {
                dialog.getDialogPane().setContent( fxmlLoader.load() );
            } catch (IOException e) {
                System.out.println( "Couldn't load the dialog" );
                e.printStackTrace();
                return;
            }
            final AddNewLoadDialogController controller = fxmlLoader.getController();
            dialog.getDialogPane().getButtonTypes().add( ButtonType.CANCEL );
            dialog.getDialogPane().getButtonTypes().add( ButtonType.OK );
            controller.populateFields(item);

            final Button okButton = (Button) dialog.getDialogPane().lookupButton( ButtonType.OK );
            okButton.addEventFilter( ActionEvent.ACTION, ae -> {
                if (controller.checkFields()) {
                    ae.consume(); //not valid
                } else {
                }
            } );

            Optional<ButtonType> result = dialog.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                pbar.setVisible( true );
                b1.setDisable( true );
                b2.setDisable( true );
                b3.setDisable( true );
                b4.setDisable( true );
                try {
                    Task<BatchUpdateValuesResponse> task = new Task<BatchUpdateValuesResponse>() {
                        @Override
                        protected BatchUpdateValuesResponse call() throws Exception {
                            return DataSource.getInstance().editLoad( controller.processResults(), item.getLine() );
                        }
                    };
                    task.setOnSucceeded( t->{
//                        pbar.setVisible( false );
//                        b1.setDisable( false );
//                        b2.setDisable( false );
//                        b3.setDisable( false );
//                        b4.setDisable( false );
                        listLoads();
                    });
                    new Thread( task ).start();
                } catch (Exception e) {
                    pbar.setVisible( false );
                    b1.setDisable( false );
                    b2.setDisable( false );
                    b3.setDisable( false );
                    b4.setDisable( false );
                    e.printStackTrace();
                }

                System.out.println( "ok pressed" );
            } else {
                System.out.println( "cancel pressed" );
            }
        }

    }

    @FXML
    public void duplicateRow(){
        final LoadDetailsProperty loadObject = (LoadDetailsProperty) loads.getSelectionModel().getSelectedItem();
        if (loadObject != null) {
            b1.setDisable( true );
            b2.setDisable( true );
            b3.setDisable( true );
            b4.setDisable( true );
            pbar.setVisible( true );
            List returnList = new ArrayList(  );
//            String[] update1 = new String[0];
//            try {
//                update1 = new String[]{tractor.getText().trim(), load.getText().trim(),broker.getText().trim(), puLocation.getText().trim(),
//                        deLocation.getText().trim(), puDate.getValue().format( DateTimeFormatter.ofPattern("MM/dd/yyyy")),  puAppt.getText().trim() };
//            } catch (Exception e) {
//                e.printStackTrace();
//                return null;
//            }
//            String[] update2 = new String[0];
//            try {
//                update2 = new String[]{deDate.getValue().format( DateTimeFormatter.ofPattern("MM/dd/yyyy")),deAppt.getText().trim()};
//            } catch (Exception e) {
//                e.printStackTrace();
//                return null;
//            }
//            String[] update3 = new String[0];
//            try {
//                update3 = new String[]{ch1.getValue().toString(), ch2.getValue().toString(), ch3.getValue().toString(), income.getText().trim(),note.getText().trim()};
//            } catch (Exception e) {
//                e.printStackTrace();
//                return null;
//            }
            ArrayList<String> list1 = new ArrayList<>();
            ArrayList<String> list2 = new ArrayList<>();
            ArrayList<String> list3 = new ArrayList<>();

            if (loadObject.getTractor() != null) {
                list1.add(loadObject.getTractor());
            } else {
                list1.add( "" );
            }
            if (loadObject.getLoad() != null) {
                list1.add( loadObject.getLoad() );
            } else {
                list1.add( "" );
            }
            if (loadObject.getBroker() != null) {
                list1.add( loadObject.getBroker() );
            } else {
                list1.add( "" );
            }
            if (loadObject.getPuLocation() != null) {
                list1.add( loadObject.getPuLocation() );
            } else {
                list1.add( "" );
            }
            if (loadObject.getDeLocation() != null) {
                list1.add( loadObject.getDeLocation() );
            } else {
                list1.add( "" );
            }
            if (loadObject.getPuDate() != null) {
                list1.add( loadObject.getPuDate() );
            } else {
                list1.add( "" );
            }

            if (loadObject.getPuAppt() != null) {
                list1.add( loadObject.getPuAppt() );
            } else {
                list1.add( "" );
            }

            if (loadObject.getDeDate() != null) {
                list2.add( loadObject.getDeDate() );
            } else {
                list2.add( "" );
            }

            if (loadObject.getDeAppt() != null) {
                list2.add( loadObject.getDeAppt() );
            } else {
                list2.add( "" );
            }

            if (loadObject.getLumper()  != null) {
                list3.add( loadObject.getLumper() );
                System.out.println(loadObject.getLumper());
            } else {
                list3.add( "" );
            }
            if (loadObject.getDetention() != null) {
                list3.add( loadObject.getDetention() );
                System.out.println(loadObject.getDetention());
            } else {
                list3.add( "" );
            }
            if (loadObject.getStatus() != null) {
                list3.add( loadObject.getStatus() );
                System.out.println(loadObject.getStatus());
            } else {
                list3.add( "" );
            }

            if (loadObject.getIncome() != null) {
                list3.add( loadObject.getIncome() );
            } else {
                list3.add( "" );
            }
            if (loadObject.getNote() != null) {
                list3.add( loadObject.getNote() );
            } else {
                list3.add( "" );
            }

            String[] update1 = new String[list1.size()];
            for (String item: list1) {
                update1[list1.indexOf( item )] = item;
            }

            String[] update2 = new String[list2.size()];
            for (String item: list2) {
                update2[list2.indexOf( item )] = item;
            }

            String[] update3 = new String[list3.size()];
            for (String item: list3) {
                update3[list3.indexOf( item )] = item;
            }

            returnList.add( list1);
            returnList.add( list2 );
            returnList.add( list3 );
            try {
                Task<BatchUpdateValuesResponse> task = new Task<BatchUpdateValuesResponse>() {
                    @Override
                    protected BatchUpdateValuesResponse call() throws Exception {
                        return DataSource.getInstance().addNewLoad(returnList);
                    }
                };
                task.setOnSucceeded( t->{
//                    pbar.setVisible( false);
//                    b1.setDisable( false );
//                    b2.setDisable( false );
//                    b3.setDisable( false );
//                    b4.setDisable( false );
                    listLoads();
                });
                new Thread( task ).start();
            } catch (Exception e) {
                b1.setDisable( false );
                b2.setDisable( false );
                b3.setDisable( false );
                b4.setDisable( false );
                pbar.setVisible( false );
                e.printStackTrace();
            }

        }

    }
}
