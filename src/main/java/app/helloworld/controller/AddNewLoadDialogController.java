package app.helloworld.controller;

import app.helloworld.Model.DataSource;
import app.helloworld.ObjectProperty.LoadDetailsProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class AddNewLoadDialogController implements Initializable {
    @FXML
    private TextField tractor;
    @FXML
    private TextField broker;
    @FXML
    private TextField load;
    @FXML
    private TextField puLocation;
    @FXML
    private TextField deLocation;
    @FXML
    private DatePicker puDate;
    @FXML
    private TextField puAppt;
    @FXML
    private DatePicker deDate;
    @FXML
    private TextField deAppt;
    @FXML
    private TextField income;
    @FXML
    private TextField note;
    @FXML
    private TextField milesIndex;
    @FXML
    private Label milesIndexLabel;
    @FXML
    private CheckBox checkbox;
    @FXML
    private TextField distance;
    @FXML
    private Label distanceLabel;
    @FXML
    private TextField deadhead;
    @FXML
    private Label deadheadLabel;
    @FXML
    private ChoiceBox ch1;
    @FXML
    private ChoiceBox ch2;
    @FXML
    private ChoiceBox ch3;

    private Boolean miles = false;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        ch1.getItems().add("");
        ch1.getItems().add("NO");
        ch1.getItems().add("YES (US)");
        ch1.getItems().add("YES (BROKER)");
        ch1.getItems().add("YES (DRIVER)");
        ch1.getItems().add("YES + RATE (US)");
        ch1.getItems().add("YES + RATE (BROKER)");
        ch1.getItems().add("YES + RATE (DRIVER)");

        ch2.getItems().add("");
        ch2.getItems().add( "NO" );
        ch2.getItems().add( "YES" );
        ch2.getItems().add( "YES (BROKER)" );
        ch2.getItems().add( "YES + RATE" );

        ch3.getItems().add("");
        ch3.getItems().add( "CANCELLED" );
        ch3.getItems().add( "ROLLING" );
        ch3.getItems().add( "BROKE DOWN" );
        ch3.getItems().add( "PROBLEM" );
        ch3.getItems().add( "DELIVERED" );
        ch3.getItems().add( "DELIVERED + POD" );
    }
//    public AddNewLoadDialogController() {
//        ch1.getItems().add("NO");
//        ch1.getItems().add("YES (US)");
//        ch1.getItems().add("YES (BROKER)");
//        ch1.getItems().add("YES (DRIVER)");
//        ch1.getItems().add("YES + RATE (US)");
//        ch1.getItems().add("YES + RATE (BROKER)");
//        ch1.getItems().add("YES + RATE (DRIVER)");
//
//        ch2.getItems().add( "NO" );
//        ch2.getItems().add( "YES" );
//        ch2.getItems().add( "YES (BROKER)" );
//        ch2.getItems().add( "NO" );
//        ch2.getItems().add( "YES + RATE" );
//
//        ch3.getItems().add( "CANCELLED" );
//        ch3.getItems().add( "ROLLING" );
//        ch3.getItems().add( "BROKE DOWN" );
//        ch3.getItems().add( "PROBLEM" );
//        ch3.getItems().add( "DELIVERED" );
//        ch3.getItems().add( "DELIVERED + POD" );
//    }

    public List<List<List<String>>> processResults(){
        List returnList = new ArrayList(  );
        String[] update1 = new String[0];
        try {
            update1 = new String[]{tractor.getText().trim(), load.getText().trim(),broker.getText().trim(), puLocation.getText().trim(),
                    deLocation.getText().trim(), puDate.getValue().format( DateTimeFormatter.ofPattern("MM/dd/yyyy")),  puAppt.getText().trim() };
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        String[] update2 = new String[0];
        try {
            update2 = new String[]{deDate.getValue().format( DateTimeFormatter.ofPattern("MM/dd/yyyy")),deAppt.getText().trim()};
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        String[] update3 = new String[0];
        try {
            update3 = new String[]{ch1.getValue().toString(), ch2.getValue().toString(), ch3.getValue().toString(), income.getText().trim(),note.getText().trim()};
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        returnList.add( Arrays.asList(update1) );
        returnList.add( Arrays.asList(update2) );
        returnList.add( Arrays.asList(update3) );
        if (checkbox.isSelected()){
            processMiles();
        }
        return returnList;
    }

    public void processMiles(){
        List returnList = new ArrayList(  );
        String[] update1=null;
        String[] update2=null;
        try {
            update1 = new String[]{puDate.getValue().format( DateTimeFormatter.ofPattern("M/d/yyyy")),puLocation.getText().trim(),
                    deDate.getValue().format(DateTimeFormatter.ofPattern("M/d/yyyy")), deLocation.getText().trim(), income.getText().trim()};
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            update2 = new String[]{distance.getText().trim(), deadhead.getText().trim()};
        } catch (Exception e) {
            e.printStackTrace();
        }
        returnList.add( Arrays.asList(update1) );
        returnList.add( Arrays.asList(update2) );
        String str =milesIndex.getText();
        String[] part = str.split("(?<=\\D)(?=\\d)");
        //System.out.println(part[0]);
        //System.out.println(part[1]);
        try {
            DataSource.getInstance().addNewLoadToMilesTable(returnList, part[0], Integer.parseInt( part[1]));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public boolean checkFields(){
        if (puDate.getValue().toString().trim().length() == 0 || deDate.getValue().toString().trim().length() == 0){
            return true;
        } else if (checkbox.isSelected()){
            if (milesIndex.getText().trim().length() == 0 || distance.getText().trim().length() == 0 ||
            deadhead.getText().trim().length() == 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void toggleMiles(){
        if(miles){
            miles=false;
            milesIndex.setVisible( false );
            milesIndexLabel.setVisible( false );
            distance.setVisible( false );
            distanceLabel.setVisible( false );
            deadhead.setVisible( false );
            deadheadLabel.setVisible( false );
        } else {
            miles = true;
            milesIndex.setVisible( true );
            milesIndexLabel.setVisible( true );
            distance.setVisible( true );
            distanceLabel.setVisible( true );
            deadhead.setVisible( true );
            deadheadLabel.setVisible( true );
        }
    }

    public void populateFields(LoadDetailsProperty loadObject){
        checkbox.setVisible( false );
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");
        if (loadObject.getTractor() != null) {
            tractor.setText( loadObject.getTractor());
        }
        if (loadObject.getBroker() != null) {
            broker.setText( loadObject.getBroker() );
        }
        if (loadObject.getLoad() != null) {
            load.setText( loadObject.getLoad() );
        }
        if (loadObject.getPuLocation() != null) {
            puLocation.setText( loadObject.getPuLocation() );
        }
        if (loadObject.getDeLocation() != null) {
            deLocation.setText( loadObject.getDeLocation() );
        }
        if (loadObject.getPuDate() != null) {
            try {
                puDate.setValue( LocalDate.parse( loadObject.getPuDate(), formatter ) );
            } catch (Exception e ){
                e.printStackTrace();
            }
        }

        if (loadObject.getDeDate() != null) {
            try {
                deDate.setValue( LocalDate.parse( loadObject.getDeDate(), formatter ) );
            } catch (Exception e ){
                e.printStackTrace();
            }
        }

        if (loadObject.getPuAppt() != null) {
            puAppt.setText( loadObject.getPuAppt() );
        }
        if (loadObject.getDeAppt() != null) {
            deAppt.setText( loadObject.getDeAppt() );
        }
        if (loadObject.getIncome() != null) {
            income.setText( loadObject.getIncome() );
        }
        if (loadObject.getNote() != null) {
            note.setText( loadObject.getNote() );
        }
        if (loadObject.getLumper()  != null) {
            ch1.setValue( loadObject.getLumper() );
        }
        if (loadObject.getDetention() != null) {
            ch2.setValue( loadObject.getDetention() );
        }
        if (loadObject.getStatus() != null) {
            ch3.setValue( loadObject.getStatus() );
        }


    }
}
