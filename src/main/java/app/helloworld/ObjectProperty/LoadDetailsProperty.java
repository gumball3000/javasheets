package app.helloworld.ObjectProperty;

import javafx.beans.property.SimpleStringProperty;

public class LoadDetailsProperty {
    private SimpleStringProperty line;
    private SimpleStringProperty tractor;
    private SimpleStringProperty load;
    private SimpleStringProperty broker;
    private SimpleStringProperty puLocation;
    private SimpleStringProperty deLocation;
    private SimpleStringProperty puDate;
    private SimpleStringProperty puAppt;
    private SimpleStringProperty puIn;
    private SimpleStringProperty puOut;
    private SimpleStringProperty deDate;
    private SimpleStringProperty deAppt;
    private SimpleStringProperty deIn;
    private SimpleStringProperty deOut;
    private SimpleStringProperty lumper;
    private SimpleStringProperty detention;
    private SimpleStringProperty status;
    private SimpleStringProperty income;
    private SimpleStringProperty note;

    public LoadDetailsProperty() {
        this.line = new SimpleStringProperty(  );
        this.tractor = new SimpleStringProperty(  );
        this.load = new SimpleStringProperty(  );
        this.broker = new SimpleStringProperty(  );
        this.puLocation = new SimpleStringProperty(  );
        this.deLocation = new SimpleStringProperty(  );
        this.puDate = new SimpleStringProperty(  );
        this.puAppt = new SimpleStringProperty(  );
        this.puIn = new SimpleStringProperty(  );
        this.puOut = new SimpleStringProperty(  );
        this.deDate = new SimpleStringProperty(  );
        this.deAppt = new SimpleStringProperty(  );
        this.deIn = new SimpleStringProperty(  );
        this.deOut = new SimpleStringProperty(  );
        this.lumper = new SimpleStringProperty(  );
        this.detention = new SimpleStringProperty(  );
        this.status = new SimpleStringProperty(  );
        this.income = new SimpleStringProperty(  );
        this.note = new SimpleStringProperty(  );
    }

    public String getLine() {
        return line.get();
    }

    public SimpleStringProperty lineProperty() {
        return line;
    }

    public void setLine(String line) {
        this.line.set( line );
    }

    public String getTractor() {
        return tractor.get();
    }

    public SimpleStringProperty tractorProperty() {
        return tractor;
    }

    public void setTractor(String tractor) {
        this.tractor.set( tractor );
    }

    public String getLoad() {
        return load.get();
    }

    public SimpleStringProperty loadProperty() {
        return load;
    }

    public void setLoad(String load) {
        this.load.set( load );
    }

    public String getPuLocation() {
        return puLocation.get();
    }

    public SimpleStringProperty puLocationProperty() {
        return puLocation;
    }

    public void setPuLocation(String puLocation) {
        this.puLocation.set( puLocation );
    }

    public String getDeLocation() {
        return deLocation.get();
    }

    public SimpleStringProperty deLocationProperty() {
        return deLocation;
    }

    public void setDeLocation(String deLocation) {
        this.deLocation.set( deLocation );
    }

    public String getPuDate() {
        return puDate.get();
    }

    public SimpleStringProperty puDateProperty() {
        return puDate;
    }

    public void setPuDate(String puDate) {
        this.puDate.set( puDate );
    }

    public String getPuAppt() {
        return puAppt.get();
    }

    public SimpleStringProperty puApptProperty() {
        return puAppt;
    }

    public void setPuAppt(String puAppt) {
        this.puAppt.set( puAppt );
    }

    public String getPuIn() {
        return puIn.get();
    }

    public SimpleStringProperty puInProperty() {
        return puIn;
    }

    public void setPuIn(String puIn) {
        this.puIn.set( puIn );
    }

    public String getPuOut() {
        return puOut.get();
    }

    public SimpleStringProperty puOutProperty() {
        return puOut;
    }

    public void setPuOut(String puOut) {
        this.puOut.set( puOut );
    }

    public String getDeDate() {
        return deDate.get();
    }

    public SimpleStringProperty deDateProperty() {
        return deDate;
    }

    public void setDeDate(String deDate) {
        this.deDate.set( deDate );
    }

    public String getDeAppt() {
        return deAppt.get();
    }

    public SimpleStringProperty deApptProperty() {
        return deAppt;
    }

    public void setDeAppt(String deAppt) {
        this.deAppt.set( deAppt );
    }

    public String getDeIn() {
        return deIn.get();
    }

    public SimpleStringProperty deInProperty() {
        return deIn;
    }

    public void setDeIn(String deIn) {
        this.deIn.set( deIn );
    }

    public String getDeOut() {
        return deOut.get();
    }

    public SimpleStringProperty deOutProperty() {
        return deOut;
    }

    public void setDeOut(String deOut) {
        this.deOut.set( deOut );
    }

    public String getLumper() {
        return lumper.get();
    }

    public SimpleStringProperty lumperProperty() {
        return lumper;
    }

    public void setLumper(String lumper) {
        this.lumper.set( lumper );
    }

    public String getDetention() {
        return detention.get();
    }

    public SimpleStringProperty detentionProperty() {
        return detention;
    }

    public void setDetention(String detention) {
        this.detention.set( detention );
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set( status );
    }

    public String getIncome() {
        return income.get();
    }

    public SimpleStringProperty incomeProperty() {
        return income;
    }

    public void setIncome(String income) {
        this.income.set( income );
    }

    public String getNote() {
        return note.get();
    }

    public SimpleStringProperty noteProperty() {
        return note;
    }

    public void setNote(String note) {
        this.note.set( note );
    }

    public String getBroker() {
        return broker.get();
    }

    public SimpleStringProperty brokerProperty() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker.set( broker );
    }
}
